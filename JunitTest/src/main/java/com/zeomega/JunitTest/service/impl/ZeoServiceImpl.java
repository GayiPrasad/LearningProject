package com.zeomega.JunitTest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.zeomega.JunitTest.service.ZeoService;

@Service("ZeoServiceImpl")
public class ZeoServiceImpl implements ZeoService {

	@Override
	public boolean isValid(String input) {
		// TODO Auto-generated method stub
		if(!StringUtils.isEmpty(input) && input.equalsIgnoreCase("zeomega")){
			return true;
		}else{
			return false;
		}
	}

}
