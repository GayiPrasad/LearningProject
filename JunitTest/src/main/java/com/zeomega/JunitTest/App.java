package com.zeomega.JunitTest;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.zeomega.JunitTest"})
public class App {

	public static void main(String[] args) {
		
	}
}
