package com.zeomega.JunitTest;

//import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.zeomega.JunitTest.service.ZeoService;
import com.zeomega.JunitTest.service.impl.ZeoServiceImpl;

/**
 * Unit test for simple App.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { App.class })
public class AppTest {

	@Autowired
	@Qualifier("ZeoServiceImpl")
	ZeoService zeoService;

	public static final String ZEO = "zeomega";

	@Rule
	public ErrorCollector collector = new ErrorCollector();

	@Test
	public void test_ml() {
		// assert correct type/impl
		assertThat(zeoService, instanceOf(ZeoServiceImpl.class));
	}

	@Test
	public void test_m2() {
		// assert true
		assertThat(zeoService.isValid(ZEO), is(true));
	}

	@Test
	public void test_m3() {
		assertTrue(zeoService.isValid(ZEO));
	}

	@Test
	public void test_m4() {
		// Assert.fail();
		if (!zeoService.isValid(ZEO)) {
			fail("This is a bad req");
		}
	}

	@Test
	public void test_m5() {
		/*collector.addError(new Throwable("There is an error in first line"));
		collector.addError(new Throwable("There is an error in second line"));*/

		//assertTrue("A " == "B");
		System.out.println("Hello");
		try {
			//assertTrue("A " == "B");
			//assertTrue("A " == "A");
		} catch (Throwable t) {
			collector.addError(t);
		}
		System.out.println("World!!!!");
		assertTrue("A" == "A");
	}
}
